package com.framework.testcases;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.framework.design.ProjectMethods;
import com.framework.pages.CreateLeadsPage;
import com.framework.pages.EditLeadPages;
import com.framework.pages.FindLeadsPage;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;
import com.framework.pages.MyHomePage;
import com.framework.pages.MyleadsPage;
import com.framework.pages.ViewLeadPage;

public class TC003_EditLeads extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC003_EditLeads";
		testDescription = "Edit leads to leafTaps";
		testNodes = "leads";
		author = "Sudha";
		category = "smoke";
		dataSheetName = "TC003";

	}
	
	
    @Test(dataProvider = "fetchData")
	public void editLead(String cname, String fname, String lname,String userName,String password,String newCname) 
    {
		new LoginPage()
		.enterUsername(userName)
		.enterPassword(password)
		.clickLogin();
		new HomePage().clickCrmSFa();
		new MyHomePage().clickLeads();
		new MyleadsPage()
		.clickFindLeads();
		new FindLeadsPage()
		.enterFirstName(fname)
		.enterLastName(lname)
		.clickFindLeads()
		.clickFirstRecord();
		new ViewLeadPage()
		.clickEdit();
		new EditLeadPages()
		.editCompanyName("newCname")
		.clickUpdate();
		new ViewLeadPage()
		.verifyFirstName("newCname");
		

	}
	

}
