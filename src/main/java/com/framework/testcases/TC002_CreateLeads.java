package com.framework.testcases;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.framework.design.ProjectMethods;
import com.framework.pages.CreateLeadsPage;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;
import com.framework.pages.MyHomePage;
import com.framework.pages.MyleadsPage;
import com.framework.pages.ViewLeadPage;

public class TC002_CreateLeads extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLeads";
		testDescription = "Create leads to leafTaps";
		testNodes = "leads";
		author = "Sudha";
		category = "smoke";
		dataSheetName = "TC002";

	}
	
	
    @Test(dataProvider = "fetchData")
	public void createLead(String cname, String fname, String lname,String userName,String password) {
		new LoginPage()
		.enterUsername(userName)
		.enterPassword(password)
		.clickLogin();
		new HomePage().clickCrmSFa();
		new MyHomePage().clickLeads();
		new MyleadsPage().clickCreateLead();
		new CreateLeadsPage().enterCompanyName(cname).enterFirstName(fname).enterLastName(lname).clickSubmit();
		new ViewLeadPage()
		.verifyFirstName(fname);

	}
	

}
