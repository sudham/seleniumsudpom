package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadsPage extends ProjectMethods{

	public CreateLeadsPage() {
       PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	public CreateLeadsPage enterCompanyName(String data) {
				clearAndType(eleCompanyName,data);
				return this;
		
	}
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	public CreateLeadsPage enterFirstName(String data) {
		clearAndType(eleFirstName,data);
		return this;
}
	
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	public CreateLeadsPage enterLastName(String data) {
		clearAndType(eleLastName,data);
		return this;
}
	
	@FindBy(how=How.NAME,using="submitButton") WebElement eleSubmitLead;
	public ViewLeadPage clickSubmit() {
		click(eleSubmitLead);
		return new ViewLeadPage();
	
}
}