package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyleadsPage extends ProjectMethods {
	
	public MyleadsPage() {
		
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.XPATH,using="//a[text()='Create Lead']") WebElement eleCreateLead;
	public CreateLeadsPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadsPage();
	}
	
	@FindBy(how=How.XPATH,using="//a[contains(text(),'Find Leads')") WebElement elefindLead;
	public FindLeadsPage clickFindLeads() {
		click(elefindLead);	
		return new FindLeadsPage();
	}

}
