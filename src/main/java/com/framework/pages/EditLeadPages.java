package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class EditLeadPages extends ProjectMethods {

	public EditLeadPages() {
       PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="updateLeadForm_companyNam") WebElement eleEditCompanyName;
   public EditLeadPages editCompanyName(String data) {
	   clearAndType(eleEditCompanyName,data);
	   return this;
	   
   }
   
   @FindBy(how=How.XPATH,using="//input[@name='submitButton']") WebElement eleClickUpdate;
   public ViewLeadPage clickUpdate() {
	   click(eleClickUpdate);
	   return new ViewLeadPage();
	   
   }

}
