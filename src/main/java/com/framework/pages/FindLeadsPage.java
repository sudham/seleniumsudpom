package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {
	
	public FindLeadsPage() {
	       PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.XPATH,using="//label[text()='First name:']//following::input[30]") WebElement elefname;
	public FindLeadsPage enterFirstName(String data) {
		clearAndType(elefname,data);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//label[text()='Last name:']//following::input[29]") WebElement elelname;
	public FindLeadsPage enterLastName(String data) {
		clearAndType(elelname,data);
		return this;
	}
	
	
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Find Leads')]") WebElement eleFindLeads;
	public FindLeadsPage clickFindLeads() {
		click(eleFindLeads);
		return this;
	}
	
	@FindBy(how=How.XPATH,using="//table[@class='x-grid3-row-table']") WebElement eleFindFirstRow;
	public ViewLeadPage clickFirstRecord() {
		click(eleFindFirstRow);
		return new ViewLeadPage();
	}

}
